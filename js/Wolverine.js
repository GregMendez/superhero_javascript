var Wolverine = {
  Name : ['Logan', 'James'],
  SuperHeroName : 'Wolverine',
  Age : 35,
  Ennemies : ['Wendigo', 'Dents de sabre'],
  City : 'New York',
  Image : './img/Wolverine.png',
  afficherBio : function () {
    // Ajout du nom à l'élément "heroName" du html
    var node = document.createTextNode(this.SuperHeroName);
    document.getElementById('heroName').appendChild(node);

    // Ajout du lien local à l'élément "superHeroImage" du html
    document.getElementById('superHeroImage').src = this.Image;

    // Ajout du nom à l'élément "name" du html
    node = document.createTextNode(this.Name[1] + " " + this.Name[0]);
    document.getElementById('name').appendChild(node);

    // Ajout de l'age à l'élément "age" du html
    node = document.createTextNode(this.Age);
    document.getElementById('age').appendChild(node);

    // Ajout de la ville à l'élément "city" du html
    node = document.createTextNode(this.City);
    document.getElementById('city').appendChild(node);

    // Ajout de tous les ennemies à l'élément "ennemies" du html
    for (var i = 0; i < this.Ennemies.length; i++) {
      node = document.createTextNode(this.Ennemies[i]);
      document.getElementById('ennemies').appendChild(node);

      if(i < this.Ennemies.length - 1) {
        node = document.createTextNode(', ');
        document.getElementById('ennemies').appendChild(node);
      }
    }

  },
};
